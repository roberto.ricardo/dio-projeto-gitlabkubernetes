#!/bin/bash

kubectl apply -f .\mysql-deployment.yml --record
kubectl apply -f .\app-deployment.yml --record
kubectl apply -f .\load-balancer.yml 

## APLICANDO O SECRETS NO CLUSTER KUBERNETES

kubectl apply -f .\secrets.yml

